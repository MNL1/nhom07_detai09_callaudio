const express = require("express");
const { ExpressPeerServer } = require("peer");
const app = express();

app.use(express.static("public"));

app.get("/", (request, response) => {
  response.sendFile(__dirname + "/views/index.html");
});

// listen requests
const listener = app.listen(process.env.PORT, () => {
  console.log("Listening on port: " + listener.address().port);
});

// peerjs server
const peerServer = ExpressPeerServer(listener, {
  debug: true,
  path: '/myapp'
});

app.use('/peerjs', peerServer);
//
